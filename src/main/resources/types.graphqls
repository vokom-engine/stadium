# ============================================= Core entities =============================================
type Address {
    street: String
    city: String
    state: String
    zipCode: String
    country: String
}

enum Country {
    EST,
    NGA
}

enum Currency {
    EUR,
    USD,
    NRN
}

enum Language {
    EN
}

type Customer {
    firstName: String!
    lastName: String
    email: String!
    phone: PhoneNumber
    fullName: String
}

input CustomerInput {
    firstName: String!
    lastName: String
    email: String!
}

type DatePeriod {
    startDate: String
    endDate: String
}

type DateTimePeriod {
    startDateTime: String
    endDateTime: String
}

type Money {
    amount: Float
    currency: Currency
}

input MoneyInput {
    amount: Float
    currency: Currency
}

type PhoneNumber {
    number: String
    country: Country
}

input PhoneNumberInput {
    number: String!
    country: Country!
}

type TimePeriod {
    startTime: String
    endTime: String
}

# ============================================= Payment entities =============================================

type PaymentMethod {
    name: String
    url: String
    logo: String
    country: Country
    paymentType: PaymentType
}

enum PaymentType {
    BANK,
    CASH,
    CARD,
    MOBILE,
    OTHER
}

enum PaymentStatus {
    STARTED,
    PENDING,
    CANCELLED,
    EXPIRED,
    COMPLETED,
    PAID_PARTLY,
    PAID_FULLY,
    PART_REFUNDED,
    REFUNDED
}

type Transaction {
    reference: String
    customerName: String
    amountDue: Money
    dateTime: String
    paymentStatus: PaymentStatus
    country: Country
    transactionId: String
    additionalData: String
    shop: String
    signature: String
    amountPaid: Money
}

# ============================================= Product entities =============================================

type Event {
    id: ID!
    name: String
    dateTimePeriod: DateTimePeriod
    status: EventStatus
    tickets: [Ticket]
}

enum EventStatus {
    ACTIVE,
    INACTIVE,
    PAST,
    CANCELED,
    SUSPENDED
}

type Ticket {
    id: ID!
    total: Int
    available: Int
    price: Money
    type: TicketType
    category: TicketCategory
    status: TicketStatus
}

enum TicketCategory {
    STANDARD,
    VIP
}

enum TicketStatus {
    AVAILABLE,
    INACTIVE,
    SOLD_OUT
}

enum TicketType {
    FULL,
    DISCOUNTED,
    ZERO
}


# ============================================= Sales entities =============================================

type Invoice {
    id: ID
    referenceNumber: String
    dueDateTime: String
    status: InvoiceStatus
    amountDue: AmountDue
    purchaseOrder: PurchaseOrder
    paymentMethods: [PaymentMethod]
    transaction: Transaction
}

type PurchaseOrder {
    id: ID
    orderNumber: String
    orderDateTime: String
    country: Country
    language: Language
    returnUrl: String
    customer: Customer
    event: Event
    totalQuantity: Int
    totalPrice: Money
    ticketOrders: [TicketOrder]
}

input PurchaseOrderInput {
    country: Country!
    language: Language!
    returnUrl: String!
    customer: CustomerInput!
    eventId: String!
    totalQuantity: Int!
    totalPrice: MoneyInput!
    ticketOrders: [TicketOrderInput!]!
}

type Receipt {
    id: ID
    ticketUrl: String
    invoice: Invoice
    language: Language
}

type TicketOrder {
    id: ID
    ticket: Ticket
    price: Money
    quantity: Int
    discountCode: String
}

input TicketOrderInput {
    ticketId: String!
    quantity: Int!
    price: MoneyInput!
    discountCode: String
}

type AmountDue {
    total: Money
    otherCharges: Money
    tax: Money
    grandTotal: Money
}

enum InvoiceStatus {
    ACTIVE,
    INACTIVE,
    SETTLED,
    EXPIRED
}

# ============================================= Stadium entities =============================================

type League {
    id: ID!
    name: String
    datePeriod: DatePeriod
    teams: [Team]
}

#extend type Event {
#    homeTeam: Team
#    awayTeam: Team
#    stadium: Stadium
#    league: League
#}

type MatchEvent {
    id: ID!
    name: String
    dateTimePeriod: DateTimePeriod!
    status: EventStatus
    tickets: [Ticket]
    homeTeam: Team
    awayTeam: Team
    stadium: Stadium
    league: League

}

type Stadium {
    id: ID!
    name: String
    capacity: Float
    address: Address
}

type Team {
    id: ID!
    name: String
    city: String
    logo: String
}


# ============================================= Queries =============================================

type Query {
    matches(count: Int = 10, offset: Int = 0): [MatchEvent]
    match(id: String!): MatchEvent
    invoices(count: Int = 10, offset: Int = 0): [Invoice]
    invoice(id: String!): Invoice
    receipts(count: Int = 10, offset: Int = 0): [Receipt]
    receipt(id: String!): Receipt
}


# ============================================= Mutations =============================================

type Mutation {
    createOrder(orders: PurchaseOrderInput!): Invoice
}


# ============================================= Root =============================================

schema {
    query: Query
    mutation: Mutation
}








#type Mutation {
##    saveConfigProp(name: String!, value: String!) : ConfigProp!
##    saveConfigProps(props: [InConfigProp]!) : [ConfigProp]!
#}

# The Root Query for the application
#type Query {
#
#}


#type Target {
#    id: Int
#}
#
#type Scene {
#    xMin: Float
#    yMin: Float
#    xMax: Float
#    yMax: Float
#    zoneWidth: Float
#    zoneHeight: Float
#}
#
#type ConfigProp {
#    id: ID
#    name: String!
#    value: String!
#}
#
#input InConfigProp {
#    name: String!
#    value: String!
#}
#
#enum ZoneType {
#    Main
#    Queue
#    General
#}