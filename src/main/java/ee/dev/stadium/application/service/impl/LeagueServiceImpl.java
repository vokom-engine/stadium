package ee.dev.stadium.application.service.impl;

import ee.dev.modules.core.application.service.BaseGenericService;
import ee.dev.stadium.application.service.LeagueService;
import ee.dev.stadium.domain.model.League;
import ee.dev.stadium.domain.repository.LeagueRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by bilal90
 */
@Slf4j
@Service
public class LeagueServiceImpl extends BaseGenericService<LeagueRepository, League> implements LeagueService {

    @Autowired
    public LeagueServiceImpl(LeagueRepository repository) {
        super(repository);
    }
}
