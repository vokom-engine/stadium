package ee.dev.stadium.application.service.impl;

import ee.dev.modules.core.application.service.BaseGenericService;
import ee.dev.modules.product.domain.model.Event;
import ee.dev.modules.sales.application.dto.PurchaseOrderDto;
import ee.dev.modules.sales.application.service.SalesService;
import ee.dev.modules.sales.domain.model.Invoice;
import ee.dev.stadium.application.service.MatchEventService;
import ee.dev.stadium.application.service.TeamService;
import ee.dev.stadium.domain.model.MatchEvent;
import ee.dev.stadium.domain.repository.MatchEventRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by bilal90
 */
@Slf4j
@Service
public class MatchEventServiceImpl extends BaseGenericService<MatchEventRepository, MatchEvent> implements MatchEventService {

    private final TeamService teamService;
    private final SalesService salesService;

    @Autowired
    public MatchEventServiceImpl(MatchEventRepository repository, TeamService teamService, SalesService salesService) {
        super(repository);

        this.teamService = teamService;
        this.salesService = salesService;
    }

    @Override
    public List<MatchEvent> findAll(HttpServletRequest req, int count, int offset) {
        List<MatchEvent> matches = super.findAll(count, offset);

        matches(matches, req);

        return matches;
    }

    @Override
    public Invoice createOrder(PurchaseOrderDto purchaseOrder, HttpServletRequest req) {
        Event event = findOne(purchaseOrder.getEventId());
        return salesService.startOrder(purchaseOrder, event, req);
    }

    @Override
    public MatchEvent create(MatchEvent entity) {
        return super.create(entity);
    }

    @Override
    public MatchEvent update(MatchEvent entity) {
        return super.update(entity);
    }

    @Override
    public List<MatchEvent> saveAll(List<MatchEvent> entities) {
        return super.saveAll(entities);
    }

    @Override
    public MatchEvent findOne(String id) {
        return super.findOne(id);
    }

    @Override
    public List<MatchEvent> findAll() {
        return super.findAll();
    }

    private void matches(List<MatchEvent> matches, HttpServletRequest req) {
        for (MatchEvent match : matches) {
            match(match, req);
        }
    }

    private void match(MatchEvent match, HttpServletRequest req) {
        teamService.setTeamLogoPath(match.getAwayTeam(), req);
        teamService.setTeamLogoPath(match.getHomeTeam(), req);
    }
}
