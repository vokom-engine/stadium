package ee.dev.stadium.application.service.impl;

import ee.dev.modules.core.application.service.BaseGenericService;
import ee.dev.modules.core.configuration.FolderConfiguration;
import ee.dev.modules.core.utils.UrlUtil;
import ee.dev.stadium.application.service.TeamService;
import ee.dev.stadium.domain.model.Team;
import ee.dev.stadium.domain.repository.TeamRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.net.URL;

/**
 * Created by bilal90
 */
@Slf4j
@Service
public class TeamServiceImpl extends BaseGenericService<TeamRepository, Team> implements TeamService {
    private final FolderConfiguration folderConfiguration;

    @Autowired
    public TeamServiceImpl(TeamRepository repository, FolderConfiguration folderConfiguration) {
        super(repository);

        this.folderConfiguration = folderConfiguration;
    }

    public void setTeamLogoPath(Team team, HttpServletRequest req) {
        try {
            URL url = UrlUtil.getUrl(req, String.format("/files/%s", folderConfiguration.getImageFolder()));
            team.setLogo(url + team.getLogo());
        }
        catch (Exception ex){
            log.error("Error setting team logo url");
        }
    }
}
