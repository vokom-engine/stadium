package ee.dev.stadium.application.service;


import ee.dev.modules.core.application.service.GenericService;
import ee.dev.stadium.domain.model.League;

/**
 * Created by bilal90 on 10/15/2019.
 */
public interface LeagueService extends GenericService<League> {

}
