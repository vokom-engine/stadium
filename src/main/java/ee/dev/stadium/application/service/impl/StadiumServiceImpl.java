package ee.dev.stadium.application.service.impl;

import ee.dev.modules.core.application.service.BaseGenericService;
import ee.dev.stadium.application.service.StadiumService;
import ee.dev.stadium.domain.model.Stadium;
import ee.dev.stadium.domain.repository.StadiumRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by bilal90
 */
@Slf4j
@Service
public class StadiumServiceImpl extends BaseGenericService<StadiumRepository, Stadium> implements StadiumService {

    @Autowired
    public StadiumServiceImpl(StadiumRepository repository) {
        super(repository);
    }
}
