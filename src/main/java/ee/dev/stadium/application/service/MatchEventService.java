package ee.dev.stadium.application.service;

import ee.dev.modules.product.application.service.EventService;
import ee.dev.modules.sales.application.dto.PurchaseOrderDto;
import ee.dev.modules.sales.domain.model.Invoice;
import ee.dev.stadium.domain.model.MatchEvent;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by bilal90
 */
public interface MatchEventService extends EventService<MatchEvent> {

    List<MatchEvent> findAll(HttpServletRequest req, int count, int offset);

    Invoice createOrder(PurchaseOrderDto purchaseOrder, HttpServletRequest req);
}
