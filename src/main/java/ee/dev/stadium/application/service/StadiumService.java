package ee.dev.stadium.application.service;

import ee.dev.modules.core.application.service.GenericService;
import ee.dev.stadium.domain.model.Stadium;

/**
 * Created by bilal90.
 */
public interface StadiumService extends GenericService<Stadium> {
}
