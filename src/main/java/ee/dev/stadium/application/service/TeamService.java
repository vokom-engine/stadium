package ee.dev.stadium.application.service;


import ee.dev.modules.core.application.service.GenericService;
import ee.dev.stadium.domain.model.Team;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by bilal90
 */
public interface TeamService extends GenericService<Team> {
    void setTeamLogoPath(Team team, HttpServletRequest req);
}
