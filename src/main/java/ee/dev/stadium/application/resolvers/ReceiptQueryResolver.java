package ee.dev.stadium.application.resolvers;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import ee.dev.modules.sales.application.service.ReceiptService;
import ee.dev.modules.sales.domain.model.Receipt;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ReceiptQueryResolver extends BaseResolver implements GraphQLQueryResolver {

    private final ReceiptService receiptService;

    @Autowired
    public ReceiptQueryResolver(ReceiptService receiptService) {
        this.receiptService = receiptService;
    }

    public List<Receipt> getReceipts(int count, int offset, DataFetchingEnvironment env){
        return receiptService.findAll(count, offset);
    }

    public List<Receipt> getReceipts(){
        return receiptService.findAll();
    }

    public Receipt getReceipt(String id, DataFetchingEnvironment env){
        return receiptService.findOne(id);
    }
}
