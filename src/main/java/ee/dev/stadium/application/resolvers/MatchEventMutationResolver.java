package ee.dev.stadium.application.resolvers;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import ee.dev.modules.sales.application.dto.PurchaseOrderDto;
import ee.dev.modules.sales.domain.model.Invoice;
import ee.dev.stadium.application.service.MatchEventService;
import graphql.GraphQLError;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@Component("ee.peeply.dev.stadium.application.resolvers.MatchEventMutationResolver")
public class MatchEventMutationResolver extends BaseResolver implements GraphQLMutationResolver {

    private MatchEventService matchEventService;

    @Autowired
    public MatchEventMutationResolver(MatchEventService matchEventService) {
        this.matchEventService = matchEventService;
    }

    public Invoice createOrder(PurchaseOrderDto order, DataFetchingEnvironment env){
        HttpServletRequest req = BaseResolver.getHttpServletRequest(env);

        return matchEventService.createOrder(order, req);
    }

    @ExceptionHandler(Throwable.class)
    public GraphQLError handleException(Throwable e) {
        return null;//new ValidationError(e);
    }
}
