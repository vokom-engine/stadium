package ee.dev.stadium.application.resolvers;

import graphql.schema.DataFetchingEnvironment;
import graphql.servlet.GraphQLContext;

import javax.servlet.http.HttpServletRequest;

public class BaseResolver {

    public static HttpServletRequest getHttpServletRequest(DataFetchingEnvironment env){
        GraphQLContext context = env.getContext();

        return context.getHttpServletRequest().orElseThrow(() ->
                new IllegalStateException("Cannot get HttpServletRequest from context")
        );
    }
}
