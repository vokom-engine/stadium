package ee.dev.stadium.application.resolvers;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import ee.dev.modules.sales.application.service.InvoiceService;
import ee.dev.modules.sales.domain.model.Invoice;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class InvoiceQueryResolver extends BaseResolver implements GraphQLQueryResolver {

    private final InvoiceService invoiceService;

    @Autowired
    public InvoiceQueryResolver(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    public List<Invoice> getInvoices(int count, int offset, DataFetchingEnvironment env){
        return invoiceService.findAll(count, offset);
    }

    public List<Invoice> getInvoices(){
        return invoiceService.findAll();
    }

    public Invoice getInvoice(String id, DataFetchingEnvironment env){
        return invoiceService.findOne(id);
    }
}
