package ee.dev.stadium.application.resolvers;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import ee.dev.stadium.application.service.MatchEventService;
import ee.dev.stadium.domain.model.MatchEvent;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Component
public class MatchEventQueryResolver extends BaseResolver implements GraphQLQueryResolver {

    private final MatchEventService matchEventService;

    @Autowired
    public MatchEventQueryResolver(MatchEventService matchEventService) {
        this.matchEventService = matchEventService;
    }

    public List<MatchEvent> getMatches(int count, int offset, DataFetchingEnvironment env){
        HttpServletRequest req = BaseResolver.getHttpServletRequest(env);

        return matchEventService.findAll(req, count, offset);
    }

    public MatchEvent getMatch(String id, DataFetchingEnvironment env){
        return matchEventService.findOne(id);
    }
}
