package ee.dev.stadium.configuration;

import com.github.javafaker.Faker;
import com.google.common.collect.ImmutableSet;
import ee.dev.modules.core.domain.model.*;
import ee.dev.modules.product.domain.model.*;
import ee.dev.stadium.application.service.MatchEventService;
import ee.dev.stadium.domain.model.League;
import ee.dev.stadium.domain.model.MatchEvent;
import ee.dev.stadium.domain.model.Stadium;
import ee.dev.stadium.domain.model.Team;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Slf4j
@Service
public class SeedTestDb {

    private final MatchEventService matchService;

    @Autowired
    public SeedTestDb(MatchEventService matchService) {
        this.matchService = matchService;
    }

    @PostConstruct
    public void seedDb() {

        log.info("Seed database match records");

        List<MatchEvent> matches = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
            Ticket standard = new Ticket();
            standard.setTotal(7000);
            standard.setPrice(Money.of(BigDecimal.valueOf(1500), Currency.NRN));
            standard.setCategory(TicketCategory.STANDARD);
            standard.setStatus(TicketStatus.AVAILABLE);
            standard.setType(TicketType.FULL);
            standard.setTotal(ThreadLocalRandom.current().nextInt(100, 300));
            standard.setAvailable(ThreadLocalRandom.current().nextInt(50, 100));

            Ticket vip = new Ticket();
            vip.setTotal(500);
            vip.setPrice(Money.of(BigDecimal.valueOf(3500), Currency.NRN));
            vip.setCategory(TicketCategory.VIP);
            vip.setStatus(TicketStatus.AVAILABLE);
            vip.setType(TicketType.FULL);
            vip.setTotal(ThreadLocalRandom.current().nextInt(50, 80));
            vip.setAvailable(ThreadLocalRandom.current().nextInt(20, 50));

            Address address = new Address();
            Stadium stadium = new Stadium();
            League league = new League();
            MatchEvent match = new MatchEvent();
            Team home = new Team();
            Team away = new Team();

            Faker faker = new Faker();

            String city = faker.address().city();
            LocalDateTime dateTime = LocalDateTime.now().plusHours(ThreadLocalRandom.current().nextInt(0, 180));

            address.setCity(city);
            address.setState(faker.address().state());
            address.setStreet(faker.address().streetAddress());
            address.setCountry(faker.address().country());
            address.setZipCode(faker.address().zipCode());

            stadium.setName(faker.team().state() + " Stadium, " + city);
            stadium.setCapacity(Long.parseLong(faker.numerify("###")));
            stadium.setAddress(address);

            DateTimePeriod dateTimePeriod = new DateTimePeriod();
            dateTimePeriod.setStartDateTime(LocalDateTime.now());
            dateTimePeriod.setEndDateTime(LocalDateTime.now().plusHours(ThreadLocalRandom.current().nextInt(2, 5)));

            DatePeriod datePeriod = new DatePeriod();
            int start = ThreadLocalRandom.current().nextInt(0, 180);
            int end = ThreadLocalRandom.current().nextInt(start, start + 181);

            datePeriod.setStartDate(LocalDate.now().plusDays(start));
            datePeriod.setEndDate(LocalDate.now().plusDays(end));

            home.setName(faker.team().name() + " FC");
            home.setCity(faker.address().city());
            home.setLogo("7889.png");

            away.setName(faker.team().name() + " FC");
            away.setCity(faker.address().city());
            away.setLogo("50080.png");

            league.setName(faker.gameOfThrones().character() + " League");
            league.setDatePeriod(datePeriod);
            league.setTeams(ImmutableSet.of(home, away));

            match.setName(String.format("%s vs  %s", home.getName(), away.getName()));
            match.setStatus(EventStatus.ACTIVE);
            match.setLeague(league);
            match.setStadium(stadium);
            match.setDateTimePeriod(dateTimePeriod);
            match.setHomeTeam(home);
            match.setAwayTeam(away);

            match.setTickets(ImmutableSet.of(standard, vip));

            matches.add(match);
            //matchService.create(match);
        }

        matchService.saveAll(matches);

    }
}
