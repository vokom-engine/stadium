package ee.dev.stadium.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@Configuration
@EnableJpaRepositories(basePackages = {
        StadiumPackageConfig.CORE + ".domain.repository",
        StadiumPackageConfig.TICKETING + ".domain.repository",
        StadiumPackageConfig.PAYMENTS + ".domain.repository",
        StadiumPackageConfig.PRODUCT + ".domain.repository",
        StadiumPackageConfig.SALES + ".domain.repository",
        StadiumPackageConfig.STADIUM + ".domain.repository",
})
@EntityScan(basePackages = {
        StadiumPackageConfig.CORE +  ".domain",
        StadiumPackageConfig.TICKETING +  ".domain",
        StadiumPackageConfig.PAYMENTS +  ".domain",
        StadiumPackageConfig.PRODUCT +  ".domain",
        StadiumPackageConfig.SALES  +  ".domain",
        StadiumPackageConfig.STADIUM  +  ".domain",
})
@ComponentScan(basePackages = {
        StadiumPackageConfig.CORE + ".*",
        StadiumPackageConfig.TICKETING + ".*",
        StadiumPackageConfig.PAYMENTS + ".*",
        StadiumPackageConfig.PRODUCT + ".*",
        StadiumPackageConfig.SALES + ".*",
        StadiumPackageConfig.STADIUM + ".*",
})
@PropertySource("classpath:application.properties")
public class StadiumPackageConfig {

    static final String CORE = "ee.dev.modules.core";
    static final String TICKETING = "ee.dev.modules.ticketing";
    static final String PAYMENTS = "ee.dev.modules.payments";
    static final String PRODUCT = "ee.dev.modules.product";
    static final String SALES = "ee.dev.modules.sales";
    static final String STADIUM = "ee.dev.stadium";

    @Bean
    public static PropertySourcesPlaceholderConfigurer stadiumPropertyConfig() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
