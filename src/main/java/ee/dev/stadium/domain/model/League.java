package ee.dev.stadium.domain.model;

import ee.dev.modules.core.domain.model.AuditingEntity;
import ee.dev.modules.core.domain.model.DatePeriod;
import lombok.*;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by bilal90
 */
@Entity
@Data
@Table
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode(callSuper = true)
@Transactional
public class League extends AuditingEntity {

    @NonNull
    @Column(name = "name")
    private String name;

    @NonNull
    @Embedded
    private DatePeriod datePeriod;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private Set<Team> teams = new HashSet<>();
}
