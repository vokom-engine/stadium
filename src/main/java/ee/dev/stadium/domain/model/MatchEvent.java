package ee.dev.stadium.domain.model;

import ee.dev.modules.product.domain.model.Event;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by bilal90.
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class MatchEvent extends Event {

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Team homeTeam;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Team awayTeam;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Stadium stadium;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private League league;
}
