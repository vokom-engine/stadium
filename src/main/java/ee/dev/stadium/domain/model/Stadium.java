package ee.dev.stadium.domain.model;

import ee.dev.modules.core.domain.model.Address;
import ee.dev.modules.core.domain.model.AuditingEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by bilal90.
 */
@Entity
@Data
@Table
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode(callSuper = true)
public class Stadium extends AuditingEntity {

    @NonNull
    @Column(name = "name")
    private String name;

    @Column(name = "capacity")
    private long capacity;

    @NonNull
    @Embedded
    private Address address;
}
