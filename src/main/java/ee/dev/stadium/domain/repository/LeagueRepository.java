package ee.dev.stadium.domain.repository;

import ee.dev.stadium.domain.model.League;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LeagueRepository extends JpaRepository<League, String> {

}
