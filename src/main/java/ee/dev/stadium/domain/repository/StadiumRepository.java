package ee.dev.stadium.domain.repository;

import ee.dev.stadium.domain.model.Stadium;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StadiumRepository extends JpaRepository<Stadium, String> {

}
