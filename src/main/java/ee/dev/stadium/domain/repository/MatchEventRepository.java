package ee.dev.stadium.domain.repository;

import ee.dev.stadium.domain.model.MatchEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MatchEventRepository extends JpaRepository<MatchEvent, String> {

}
