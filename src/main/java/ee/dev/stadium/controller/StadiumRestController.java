package ee.dev.stadium.controller;

import ee.dev.modules.sales.application.service.SalesService;
import ee.dev.modules.web.controllers.filters.RestControllerExceptionFilter;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by bilal90.
 */
@CrossOrigin
@RestController
@RequestMapping("/api/payments")
public class StadiumRestController extends RestControllerExceptionFilter {
    private final SalesService salesService;

    @Autowired
    public StadiumRestController(SalesService salesService) {
        this.salesService = salesService;
    }

    @GetMapping("/payment-complete")
    @ResponseStatus(HttpStatus.OK)
    public void transactionComplete(HttpServletRequest request, HttpServletResponse response,
                                    @RequestParam(name = "reference") String reference) throws IOException {

        Validate.notNull(reference, "Reference cannot be null");

        String redirectUrl = salesService.transactionComplete(reference, request);

        response.sendRedirect(redirectUrl);
    }
}
